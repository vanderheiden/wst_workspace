
//Hello, world! program, for _C++ for Lazy Programmers_
//  Your name goes here
//  Then the date2
//It prints "Hello, world!" on the screen.
//    Quite an accomplishment, huh?
#include "SSDL.h"
int main (int argc, char** argv)
{
      sout << "Hello, world!  (Press any key to quit.)\n";
      SSDL_WaitKey ();      //Wait for user to hit any key
      return 0;
}”

Excerpt From: Will Briggs. “C++ for Lazy Programmers”. Apple Books.


