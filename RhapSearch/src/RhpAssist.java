import java.awt.EventQueue;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import java.awt.Font;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.border.BevelBorder;

import swing.myJFrame;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

public class RhpAssist {

	private myJFrame frame;
	private final	ButtonGroup DefaultLanguage 	= new ButtonGroup();
	private final	ButtonGroup DefaultEdition  	= new ButtonGroup();
	public 	static	Properties  myProperties    	= new Properties();	
	public  static	String		propertyFilePath 	= "c:\\";
	public	static	Font		panelHeaderFont		= new Font ("Tahoma", Font.BOLD, 12 ); 
	public	static	Font		panelPropertyFont	= new Font ("Tahoma", Font.PLAIN, 11 );
	public	static	Font		panelTitleFont		= new Font ("Tahoma", Font.BOLD, 18 );
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		
		// Check the location of the ini file.
		// Computer\HKEY_CURRENT_USER\Software\Willert\WST_RXF_V7
		// InstallationBaseDirectory

//		WinRegistry	MyReg	= new WinRegistry();

		try {
			propertyFilePath = WinRegistry.readString(WinRegistry.HKEY_CURRENT_USER, "Software\\Willert\\WST_RXF_V7", "InstallationBaseDirectory");
		}
		catch (Exception e) {
		      System.out.println(e);
		}
		// Then read in the ini file
		propertyFilePath = propertyFilePath + "\\rhapsody.ini";

		try {
			FileInputStream inPutFile = new FileInputStream(propertyFilePath);
		    myProperties.load(inPutFile);
		    inPutFile.close();
	    }
	    catch (Exception e) {
			myProperties.setProperty("DefaultEdition", "Developer");
			myProperties.setProperty("DefaultLanguage", "C++");
	      System.out.println(e);  
	    }

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RhpAssist window = new RhpAssist();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public RhpAssist() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 1008, 654);
		frame.getContentPane().add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("General", null, panel, null);
		panel.setLayout(null);
		
		JLabel lblCommonRhapsodySettings = new JLabel("Common Rhapsody \"INI\" Settings");
		lblCommonRhapsodySettings.setVerticalAlignment(SwingConstants.TOP);
		lblCommonRhapsodySettings.setHorizontalAlignment(SwingConstants.CENTER);
		lblCommonRhapsodySettings.setBounds(344, 11, 312, 22);
		lblCommonRhapsodySettings.setFont( panelTitleFont );
		panel.add(lblCommonRhapsodySettings);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel_1.setBounds(10, 50, 134, 160);
		panel.add(panel_1);
		
		JLabel lblNewLabel = new JLabel("Default Language");
		lblNewLabel.setFont( panelHeaderFont );
		lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("'C'");
		rdbtnNewRadioButton.setHorizontalAlignment(SwingConstants.LEFT);
		rdbtnNewRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				myProperties.setProperty("DefaultLanguage", "C");
			}
		});
		DefaultLanguage.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnc = new JRadioButton("'C++'");
		rdbtnc.setHorizontalAlignment(SwingConstants.LEFT);
		rdbtnc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				myProperties.setProperty("DefaultLanguage", "C++");
			}
		});
		rdbtnc.setSelected(true);
		DefaultLanguage.add(rdbtnc);
		
		JRadioButton rdbtnJava = new JRadioButton("Java");
		rdbtnJava.setHorizontalAlignment(SwingConstants.LEFT);
		rdbtnJava.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				myProperties.setProperty("DefaultLanguage", "Java");
			}
		});
		DefaultLanguage.add(rdbtnJava);
		
		JRadioButton rdbtnAda = new JRadioButton("ADA");
		rdbtnAda.setHorizontalAlignment(SwingConstants.LEFT);
		rdbtnAda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				myProperties.setProperty("DefaultLanguage", "ADA");
			}
		});
		DefaultLanguage.add(rdbtnAda);
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(20)
							.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addComponent(rdbtnc)
								.addComponent(rdbtnNewRadioButton)
								.addComponent(rdbtnJava)
								.addComponent(rdbtnAda)))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblNewLabel)))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(rdbtnNewRadioButton)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(rdbtnc)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(rdbtnJava)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(rdbtnAda)
					.addContainerGap(36, Short.MAX_VALUE))
		);
		panel_1.setLayout(gl_panel_1);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel_2.setBounds(152, 50, 190, 160);
		panel.add(panel_2);
		
		JLabel lblDefaultEdition = new JLabel("Default Edition");
		lblDefaultEdition.setFont( new Font("Tahoma", Font.BOLD, 12));
		lblDefaultEdition.setHorizontalAlignment(SwingConstants.LEFT);
		
		JRadioButton rdbtnDeveloper = new JRadioButton("Developer");
		rdbtnDeveloper.setFont( panelPropertyFont );
		rdbtnDeveloper.setSelected(true);
		DefaultEdition.add(rdbtnDeveloper);
		
		JRadioButton rdbtnDesignerForSystems = new JRadioButton("Designer for Systems");
		rdbtnDesignerForSystems.setFont( panelPropertyFont );
		DefaultEdition.add(rdbtnDesignerForSystems);
		
		JRadioButton rdbtnArchitectForSoftware = new JRadioButton("Architect for Software");
		rdbtnArchitectForSoftware.setFont( panelPropertyFont );
		DefaultEdition.add(rdbtnArchitectForSoftware);
		
		JRadioButton rdbtnArchitectForSystems = new JRadioButton("Architect for Systems");
		rdbtnArchitectForSystems.setFont( panelPropertyFont );
		DefaultEdition.add(rdbtnArchitectForSystems);
		
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(16)
							.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
								.addComponent(rdbtnDeveloper, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
								.addComponent(rdbtnDesignerForSystems, GroupLayout.PREFERRED_SIZE, 134, GroupLayout.PREFERRED_SIZE)
								.addComponent(rdbtnArchitectForSoftware)
								.addComponent(rdbtnArchitectForSystems)))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblDefaultEdition, GroupLayout.PREFERRED_SIZE, 137, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(30, Short.MAX_VALUE))
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addComponent(lblDefaultEdition, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(rdbtnDeveloper)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(rdbtnDesignerForSystems, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
					.addGap(3)
					.addComponent(rdbtnArchitectForSoftware)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(rdbtnArchitectForSystems)
					.addGap(37))
		);
		panel_2.setLayout(gl_panel_2);
		
		JButton btnScanDisk = new JButton("Scan Disk");
		btnScanDisk.setBounds(36, 683, 100, 23);
		frame.getContentPane().add(btnScanDisk);
		
		JButton btnSaveClose = new JButton("Save & Close");
		btnSaveClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					FileOutputStream outPutFile = new FileOutputStream(propertyFilePath);
				    myProperties.store( outPutFile, "Save" );
				    outPutFile.close();
				}
				catch (Exception e) {
				      System.out.println(e);
				}
				
			}
		});
		
		btnSaveClose.setBounds(864, 683, 110, 23);
		frame.getContentPane().add(btnSaveClose);
	}
}
