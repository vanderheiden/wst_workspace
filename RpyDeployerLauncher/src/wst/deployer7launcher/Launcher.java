package wst.deployer7launcher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.telelogic.rhapsody.core.IRPApplication;
import com.telelogic.rhapsody.core.IRPComponent;
import com.telelogic.rhapsody.core.IRPConfiguration;
import com.telelogic.rhapsody.core.IRPProject;
import com.telelogic.rhapsody.core.RPUserPlugin;

import wst.helper7.Helper;

public class Launcher extends RPUserPlugin
{
	private IRPApplication rpyApp = null;


	public void RhpPluginInit( IRPApplication rpyApp )
	{
		this.rpyApp = rpyApp;
		rpyApp.writeToOutputWindow( "Log", "Deployer Launcher init\n" );
	}


	/**
	 * This method is being called when the project is closed. It disconnect the
	 * suite from the registry, and remove its pointers.
	 * 
	 * @return true if the plugin should get unloaded, false otherwise
	 */
	public boolean RhpPluginCleanup()
	{
		// rpyApp.writeToOutputWindow( "Log", "RhpPluginCleanup\n" );

		rpyApp = null;
		return true;
	}


	/**
	 * This method is being called when Rhapsody exits. It must be implemented, so
	 * we do nothing.
	 */
	public void RhpPluginFinalCleanup()
	{
		// rpyApp.writeToOutputWindow( "Log", "RhpPluginFinalCleanup\n" );
	}


	/**
	 * This method is being called when the plug-in pop-up menu (if applicable) is
	 * selected. It must be implemented.
	 */
	public void OnMenuItemSelect( String menuItem )
	{
		rpyApp.writeToOutputWindow( "Log", "OnMenuItemSelect\n" );

		IRPProject activeProject = rpyApp.activeProject();
		IRPComponent activeComponent = rpyApp.activeProject().getActiveComponent();
		IRPConfiguration activeConfiguration = rpyApp.activeProject().getActiveConfiguration();

		String projectName = activeProject.getName();
		String componentName = activeComponent.getName();
		String configurationName = activeConfiguration.getName();

		String configFileName = projectName + "_" + componentName + "_" + configurationName + ".dxc";

		try
		{
			// use common configFileName for all TestConductor Components
			if ( activeConfiguration.getPropertyValue( "WSTMonitor.Deployer.Monitor" ).equals( "TestConductor" ) )
			{
				configFileName = projectName + "_Common" + ".dxc";
			}
		} catch ( Exception e )
		{
		}

		String configFilePath = activeProject.getCurrentDirectory() + "\\" + configFileName;

		String command = "\"" + Helper.resolveReleasePath( activeConfiguration ) + "\\Tools\\WSTDeployerV7\\DeployerC.exe\"";
		command += " -vm \"" + Helper.resolveJavaPath( rpyApp ) + "\"";
		command += " -logger \"de.wst.modelxchanger.core.logger.consoleLogger\"";
		command += " -loadConfig \"" + configFilePath + "\"";
		command += " -noSplash";
		command += " -showExporterSelection";
		command += " -saveConfig \"" + configFilePath + "\"";

		// rpyApp.writeToOutputWindow( "Log", "Command: \n" + command + "\n" );
		rpyApp.writeToOutputWindow( "Log", "Open deployer configuration.\n" );

		Process p;
		try
		{
			p = Runtime.getRuntime().exec( command );

			BufferedReader input = new BufferedReader( new InputStreamReader( p.getInputStream() ) );

			String line;

			while ( (line = input.readLine()) != null )
			{
				rpyApp.writeToOutputWindow( "Log", line + "\n" );
			}

			input.close();
		} catch ( IOException e )
		{
			rpyApp.writeToOutputWindow( "Log", "Failed to start deployer configuration.\n" );
			e.printStackTrace();
		}

	}


	/**
	 * This method is being called when the plug-in popup trigger (if applicable) is
	 * fired. It must be implemented, so we do nothing.
	 */
	public void OnTrigger( String trigger )
	{
		// rpyApp.writeToOutputWindow( "Log", "OnTrigger)\n" );
	}


	/**
	 * This method is being called when the plug-in menu item under the "Tools" menu
	 * is selected. It must be implemented, so we do nothing.
	 */
	public void RhpPluginInvokeItem()
	{
		// rpyApp.writeToOutputWindow( "Log", "RhpPluginInvokeItem\n" );
	}

}
