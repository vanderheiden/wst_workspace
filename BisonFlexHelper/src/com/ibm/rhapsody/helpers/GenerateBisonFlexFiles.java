package com.ibm.rhapsody.helpers;

import com.telelogic.rhapsody.core.IRPApplication;
import com.telelogic.rhapsody.core.IRPProject;
import com.telelogic.rhapsody.core.RPUserPlugin;
import com.telelogic.rhapsody.core.RhapsodyAppServer;
import com.telelogic.rhapsody.core.RhapsodyRuntimeException;


public class GenerateBisonFlexFiles extends RPUserPlugin {

	public  void init() {
		try {
			IRPApplication rpy = RhapsodyAppServer.getActiveRhapsodyApplication();
			IRPProject prj = rpy.activeProject();
			prj.getActiveComponent().locateInBrowser();
		} catch ( RhapsodyRuntimeException e) {
			System.out.println("Cannot connect to Rhapsody\n");
		}
	}
	
	private static void log(String s) {
		s = s + "\n";
		System.out.print( s );
		IRPApplication rpy = RhapsodyAppServer.getActiveRhapsodyApplication();
		rpy.writeToOutputWindow("Log", s);
	}

	@Override
	public void RhpPluginInit(IRPApplication rpyApplication) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void RhpPluginInvokeItem() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void OnMenuItemSelect(String menuItem) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void OnTrigger(String trigger) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean RhpPluginCleanup() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void RhpPluginFinalCleanup() {
		// TODO Auto-generated method stub
		
	}

}
