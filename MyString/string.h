/*
 * string.h
 *
 *  Created on: 06.10.2019
 *      Author: LocalAdministrator
 */

#ifndef STRING_H_
#define STRING_H_

#include <cstring>

namespace own {

class string;

// Greater than or equal to compare C string to an string
// Argument const char* c1 :
// The C string source
// Argument const string& s2 :
// The string to compare with
bool operator>=(const char* c1, const string& s2);

// Greater than compare C string to an string
// Argument const char* c1 :
// The C string source
// Argument const string& s2 :
// The string to compare with
bool operator>(const char* c1, const string& s2);

// Equal to compare C string to an string
// Argument const char* c1 :
// The C string source
// Argument const string& s2 :
// The string to compare with
bool operator==(const char* c1, const string& s2);

// Less than or equal to compare C string to an string
// Argument const char* c1 :
// The C string source
// Argument const string& s2 :
// The string to compare with
bool operator<=(const char* c1, const string& s2);

// Less than compare C string to an string
// Argument const char* c1 :
// The C string source
// Argument const string& s2 :
// The string to compare with
bool operator<(const char* c1, const string& s2);

// Add stings
// Argument const string& s1 :
// A string to add
// Argument const string& s2 :
// A string to add
string operator+(const string& s1, const string& s2);

// Add stings
// Argument const string& s1 :
// A string to add
// Argument const char* s2 :
// A string to add
string operator+(const string& s1, const char* s2);

// Add stings
// Argument const char* s1 :
// The C string
// Argument const string& s2 :
// The string
string operator+(const char* s1, const string& s2);

// not equal test
// Argument const char* c1 :
// The source
// Argument const string& s2 :
// Compared string
bool operator!=(const char* c1, const string& s2);

// Add a sting and a character
// Argument const string& str :
// A string to add
// Argument const char ch :
// The character
string operator+(const string& str, const char ch);



class string {

public:

    // Initialize a string based on another string
    // Argument const string& s1 :
    // The source string
    string(const string& s1);

    // Initialize a string based on a single character
    // Argument const char ch :
    // The character
    string(const char ch);

    // Initialize a string based on another string (C style)
    // Argument const char* st :
    // The C string
    string(const char* st);

    // Initialize an empty string
    string(void);

    // Cleanup
    ~string(void);

    // empty the string
    void Empty(void);

    // get a character at a given position
    // Argument int ind :
    // The index
    char GetAt(int ind);

    // get the string buffer
    // Argument int /**/ :
    // dummy
    char* GetBuffer(int /**/) const;

    // get the string buffer, readjusting its size.
    // Argument int newBufferSize :
    // The new buffer minimal size
    char* GetBuffer(int newBufferSize);

    // check if string is empty
    bool IsEmpty(void) const;

    // Release buffer sets the count value. It must be called before any use of string, if GetBuffer was used for direct change.
    // Argument int nNewLength = -1 :
    // The new string count. It can be called without argument, if buffer string ends with null character.
    void ReleaseBuffer(int nNewLength = -1);

    // set a character at a given position
    // Argument int ind :
    // The index
    // Argument char ch :
    // The character
    void SetAt(int ind, char ch);

    // cast operator
    operator const char*(void);

    // not equal test with a C string
    // Argument const char* c2 :
    // The C string
    bool operator!=(const char* c2) const;

    // not equal test with a string
    // Argument const string& s2 :
    // The string
    bool operator!=(const string& s2) const;

    // Add a C style string to the end of this string
    // Argument const char* st :
    // The string
    const string& operator+=(const char* st);

    // Add a character to the end of this string
    // Argument const char ch :
    // The character
    const string& operator+=(const char ch);

    // Add a string to the end of this string
    // Argument const string& st :
    // The string
    const string& operator+=(const string& st);

    // Less than test
    // Argument const char* c2 :
    // The context string
    bool operator<(const char* c2) const;

    // Less than test
    // Argument const string& s2 :
    // The context
    bool operator<(const string& s2) const;

    // Less than or equal to test
    // Argument const char* c2 :
    // The context
    bool operator<=(const char* c2) const;

    // Less than or equal to test
    // Argument const string& s2 :
    // The context
    bool operator<=(const string& s2) const;

    // Initialize a string based on another std::string
    // Argument const std::string& s1 :
    // The source string ( from <string> )
    const string& operator=(const std::string s1);


    // Assign the specified string as the value of this string
    // Argument const char* st :
    // The context
    const string& operator=(const char* st);

    // Assign the specified character as the value of this string
    // Argument const char ch :
    // The context
    const string& operator=(const char ch);

    // Assign the specified string as the value of this string
    // Argument const string& st :
    // The context
    const string& operator=(const string& st);

    // Compare this string with the specified string
    // Argument const char* c2 :
    // The context
    bool operator==(const char* c2) const;

    // Compare this string with the specified string
    // Argument const string& s2 :
    // The context
    bool operator==(const string& s2) const;

    // Greater than test
    // Argument const char* c2 :
    // The context
    bool operator>(const char* c2) const;

    // Greater than test
    // Argument const string& s2 :
    // The context
    bool operator>(const string& s2) const;

    // Greater than or equal to test
    // Argument const char* c2 :
    // The context
    bool operator>=(const char* c2) const;

    // Greater than or equal to test
    // Argument const string& s2 :
    // The context
    bool operator>=(const string& s2) const;

    // return the character at the given position
    // Argument int ind :
    // The index
    char operator[](int ind) const;

    // give string a new larger size
    // and copy contents to it.
    // Argument int newSize :
    // The new buffer minimal size
    void resetSize(int newSize);

private :

    // allocate the string buffer
    // Argument int newSize = defaultBlock :
    // The new buffer minimal size
    void setSize(int newSize = defaultBlock);

public :

    int GetLength(void) const;

    int getDefaultBlock(void);

    int getSize(void) const;

    char* getStr(void) const;

private :

    // No case compare
    // Argument const char* const s1 :
    // The source string
    // Argument const char* const s2 :
    // The string to compare with
    static int CompareNoCase_(const char* const s1, const char* const s2);

public :

    // No case compare
    // Argument const char* const s2 :
    // The string to compare with
    int CompareNoCase(const char* const s2) const;

    // No case compare
    // Argument const string& s2 :
    // The string to compare with
    int CompareNoCase(const string& s2) const;

    static int toupper(const int character);

    ////    Attributes    ////

private :

    // How many chars we currently have (without the '\0')
    int count;

    // the string default size
    // need to be declared before used (in this file) to avoid compilation issues in some compilers
    static int defaultBlock;

    // The current allocated memory
    int size;

    // Pointer to actual string
    char* str;
};

};

#endif
