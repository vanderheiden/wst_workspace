/*
 * main.c
 *
 *  Created on: 06.10.2019
 *      Author: LocalAdministrator
 */

#include "string.h"
#include <string>

int main(void)
{
	// assume your string implementation is defined in namespace ::own own::string str0; //constructs a default (empty) string
	auto str1 = own::string("alice"); // constructs a string with the word alice str0 = str1; // copy assignment: str0 now contains "alice"

	auto str2(str1); // copy construction: str2 is a new string that contains "alice"
	std::string sstr0 = "bob";

	own::string str3 = sstr0; //converting constructor from std::string std::string sstr1 = str0.s_str(); //conversion back to std::string
	const char* c_str = str0.c_str(); //conversion to const char*
	bool eq1 = str0 == str1; // equality comparison, evaluates to true
	bool neq1 = str0 != "bob";
	own::string str4 = "three, ";
	own::string str5 = "two, ";
	own::string str6 = "one.";
	// similarly - evaluates also to true
	str4 += str5; //concatenation str4 == "three, two, "
	auto str7 = str4 + str6 + ".."; // again a concatenation followed by an assignment;
	//str7 == "three, two, one..." char c = str7[4]; // indexed access to characters, c == 'e'
	std::ofstream fout{"file"};
	fout << str7; // stream output operator std::cout << str0; // same as above
	Moreover, your string class must implement the following functions that are present in the std::string: bool own::string::empty() const;
	int own::string::size() const;
	And the following functions that are missing from the original std::string:
	// returns a capitalized copy of the string:
	own::string own::string::to_upper() const; // returns a lower-case copy of the string:
	own::string own::string::to_lower() const;
	// returns true if string contains a substring passed as an argument
	bool contains(const own::string& substr) const;
	// returns true if string string a substring passed as an argument
	1
	bool starts_with(const own::string& substr) const;
	// returns true if string ends with a substring passed as an argument
	bool ends_with(const own::string& substr) const;
	// returns a copy stripped of any white-space characters in the beginning and the end
	own::string strip() const;
	//Example:
	own::string str = " Alice ";
	auto stripped = str.strip(); // stripped == "Alice"
	// prints: "alice ALICE":
	std::cout << stripped.to_lower() << " " << stripped.to_upper();
	auto a = str.contains("lic"); //true; auto b = str.starts_with("al"); //false auto c = str.ends_with("ice "); //true
}



