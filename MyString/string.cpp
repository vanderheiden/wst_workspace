/*
 * string.cpp
 *
 *  Created on: 06.10.2019
 *      Author: LocalAdministrator
 */

#include "string.h"

namespace own {

bool operator>=(const char* c1, const string& s2)
{
    return (std::strcmp(c1,s2.GetBuffer(0)) >= 0);
}

bool operator>(const char* c1, const string& s2)
{
    return (std::strcmp(c1,s2.GetBuffer(0)) > 0);
}

bool operator==(const char* c1, const string& s2)
{
    return (std::strcmp(c1,s2.GetBuffer(0)) == 0);
}

bool operator<=(const char* c1, const string& s2)
{
    return (std::strcmp(c1,s2.GetBuffer(0)) <= 0);
}

bool operator<(const char* c1, const string& s2)
{
    return (std::strcmp(c1,s2.GetBuffer(0)) < 0);
}

string operator+(const string& s1, const string& s2)
{
    string res(s1);
    res += s2;
    return res;
}

string operator+(const string& s1, const char* s2)
{
    string res(s1);
    res += s2;
    return res;
}

string operator+(const char* s1, const string& s2)
{
    string res(s1);
    res += s2;
    return res;
}

bool operator!=(const char* c1, const string& s2)
{
    return (std::strcmp(c1, s2.GetBuffer(0)) != 0);
}

string operator+(const string& str, const char ch)
{
    string res(str);
    res += ch;
    return res;
}

int string::defaultBlock = 256;

string::string(const string& s1) : count(0), size(0), str(0)
{
    //lint -save -e668 -e613 (lint fail to identify the memory allocation by setSize())
    count = s1.count;
    setSize( count + 1 );
    if ( count > 0 ){
    	std::memcpy( str, s1.str, (unsigned int) count + 1 );
    }
    else {
    	str[count] = '\0';
    }
    // lint -restore
}

string::string(const std::string& s1) : count(0), size(0), str(0)
{
	count = s1.size();
    setSize( count + 1 );
    if ( count > 0 ){
    	std::memcpy( str, s1.str, (unsigned int) count + 1 );
    }
    else {
    	str[count] = '\0';
    }
}

string::string(const char ch) : count(0), size(0), str(0)
{
    count = 1;
    setSize(count + 1);
    //lint -save -e613 (lint fail to identify the memory allocation by setSize())
    str[0] = ch;
    str[count] = '\0';
    //lint -restore
}

string::string(const char* st) : count(0), size(0), str(0)
{
    //lint -save  -e668 -e613 (lint fails to identify the memory allocation by setSize())
    if (st != 0) {
    	count = static_cast<int>(strlen(st));
    	setSize(count + 1);
    	if (count > 0) {
    		std::memcpy(str, st, strlen(st) + 1);
    	}
    	else {
    		str[count] = '\0';
    	}
    }
    else{
    	setSize(count + 1);
    	str[count]='\0';
    }
    //lint -restore
}

string::string(void) : count(0), size(0), str(0)
{
    setSize(1);
    //lint -save -e613 (lint fail to identify the memory allocation by setSize())
    str[0] = '\0';
    // lint -restore
}

string::~string(void)
{
    if (size > 0) {
    	delete str;
    }
    str = NULL;
    size = 0;
    count = 0;
}

void string::Empty(void)
{
    if (count > 0)
    {
    	count = 0;
    	// lint fails to identify the correlation between count and str
    	str[count]='\0';  //lint !e613
    }
}

char string::GetAt(int ind)
{
    char retVal = '\0';
    if (ind > -1 && ind < count)
    {
    	retVal = str[ind];
    }
    return retVal;
}

char* string::GetBuffer(int /**/) const
{
    return str;
}

char* string::GetBuffer(int newBufferSize)
{
    resetSize(newBufferSize);
    return str;
}

bool string::IsEmpty(void) const
{
    return count == 0;
}

void string::ReleaseBuffer(int nNewLength)
{
    if (nNewLength == -1)
    {
    	count = static_cast<int>(strlen(str));
    }
    else
    {
    	if (nNewLength <= size)
    	{
    		count = nNewLength;
    		if (count < size)
    		{
    			str[count] = '\0';
    		}
    	}
    }
}

void string::SetAt(int ind, char ch)
{
    if (ind < count)
    {
    	// lint fails to identify the correlation between count and str
    	str[ind] = ch;	//lint !e613
    }
}

// the operation is not const to comply with MFC CString
//lint -save -e1762
string::operator const char*(void)
{
    return str;
}

//lint -restore

bool string::operator!=(const char* c2) const
{
    return (strcmp(str, c2) != 0);  //lint !e668 (string::str is never NULL)
}

bool string::operator!=(const string& s2) const
{
    return (strcmp(str, s2.str) != 0); //lint !e668 (string::str is never NULL)
}

const string& string::operator+=(const char* st)
{
    if(st){
    	int newCount = count + static_cast<int>(strlen(st));
    	resetSize(newCount + 1);
    	if(str){
    		memcpy(str + count, st, strlen(st) + 1);
    		count = newCount;
    	}
    }
    return *this;
}

const string& string::operator+=(const char ch)
{
    ++count;
    resetSize(count + 1);
    if(str) {
    	str[count-1] = ch;
    	str[count] = '\0';
    }
    return *this;
}

const string& string::operator+=(const string& st)
{
    int newCount = count + st.count;
    resetSize(newCount + 1);
    if (str) {
    	memcpy(str + count, st.str, (unsigned int) st.count + 1);
    	count = newCount;
    }
    return *this;
}

bool string::operator<(const char* c2) const
{
    bool smaller = false;
    if (c2 != 0) {
    	smaller = (strcmp(str,c2) < 0); //lint !e668 (str is never NULL)
    }
    return smaller;
}

bool string::operator<(const string& s2) const
{
    return (strcmp(str, s2.str) < 0); //lint !e668 (string::str is never NULL)
}

bool string::operator<=(const char* c2) const
{
    bool res = false;
    if (c2 != 0) {
    	res = (strcmp(str, c2) <= 0); //lint !e668 (str is never NULL)
    }
    return res;
}

bool string::operator<=(const string& s2) const
{
    return (strcmp(str, s2.str) <= 0);	//lint !e668 (string::str is never NULL)
}

const string& string::operator=(const char* st)
{
    if (st) {
    	count = static_cast<int>(strlen(st));
    	resetSize(count + 1);
    	if (str) {
    		memcpy(str, st, strlen(st) + 1);
    	}
    }
    return *this;
}

//lint -save -e1539 (the size doesn't change only the data)
const string& string::operator=(const char ch)
{
    count = 1;
    if (str) {
    	str[0] = ch;
    	str[count] = '\0';
    }
    return *this;
}

//lint -restore

const string& string::operator=(const string& st)
{
    if(str != st.str) {
    	count = st.count;
    	resetSize(count + 1);
    	if (str) {
    		memcpy(str, st.str, static_cast<unsigned int>(count) + 1);
    	}
    }
    return *this;
}

bool string::operator==(const char* c2) const
{
    bool res = false;
    if (c2 != 0) {
    	res = (strcmp(str, c2) == 0); //lint !e668 (str is never NULL)
    }
    return res;
}

bool string::operator==(const string& s2) const
{
    return (strcmp(str, s2.str) == 0); //lint !e668 (str is never NULL)
}

bool string::operator>(const char* c2) const
{
    bool res = true;
    if (c2 != 0) {
    	res = (strcmp(str, c2) > 0); //lint !e668 (str is never NULL)
    }
    return res;
}

bool string::operator>(const string& s2) const
{
    return (strcmp(str, s2.str) > 0);	//lint !e668 (str is never NULL)
}

bool string::operator>=(const char* c2) const
{
    bool res = true;
    if (c2 != 0) {
    	res = (strcmp(str, c2) >= 0);	//lint !e668 (str is never NULL)
    }
    return res;
}

bool string::operator>=(const string& s2) const
{
    return (strcmp(str, s2.str) >= 0);	//lint !e668 (str is never NULL)
}

char string::operator[](int ind) const
{
    return str[ind];
}

void string::resetSize(int newSize)
{
    // Do we need to allocate new memory
    if (size < newSize) {
    	// retain a pointer to the actual data
    	char* oldStr = str;
    	// allocate the new memory
    	int oldSize = size;
    	setSize(newSize);
    	// copy the actual data to the new location
    	if (str && oldStr) {
    		memcpy(str,oldStr, (unsigned int)oldSize);
    	}
    	// free memory used by the old location
    	delete oldStr ;
    }
}

void string::setSize(int newSize)
{

    // Round size up to the nearest stringBlock
    size = (newSize / defaultBlock) * defaultBlock;
    if (newSize > size) {
    	size += defaultBlock;
    }
    // Actually allocate the memory
    str = new char [ size ];
}

int string::GetLength(void) const
{
    return count;
}

int string::getDefaultBlock(void)
{
    return defaultBlock;
}

int string::getSize(void) const
{
    return size;
}

char* string::getStr(void) const
{
    return str;
}

int string::CompareNoCase_(const char* const s1, const char* const s2)
{
    // res = 3 - unknown
    int res = 3;
    if ((s1 != 0) && (s2 != 0)) {
    	for (int i = 0; res == 3; i++) {
    		char d1 = (char)toupper(s1[i]);
    		char d2 = (char)toupper(s2[i]);
    		if (d1 < d2) {
    			res = -1;
    		}
    		else if (d1 > d2) {
    			res = 1;
    		}
    		else if (d1 == '\0') {
    			res = 0;
    		}
    	}
    }
    return res;
}

int string::CompareNoCase(const char* const s2) const
{
    return CompareNoCase_(str,s2);
}

int string::CompareNoCase(const string& s2) const
{
    return CompareNoCase_(str, s2.str);
}

int string::toupper(const int character)
{
    int res = character;
    if ((character >= 97) && (character <= 122)) {
    	res = (character - 32);
    }
    return res;
}


} /* namespace own */
