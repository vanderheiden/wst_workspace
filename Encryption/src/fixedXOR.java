
public class fixedXOR {

		private byte Input1[], Input2[];
		private int	 inLength = 0;
		private	int	 outLength = 0;
		public  byte Output[];

		public fixedXOR (String s1, String s2)
		{
			int i = 0;
			
			inLength = s1.length()/2;
			outLength = inLength;

			Input1 = new byte [inLength];
			Input2 = new byte [inLength];
			Output = new byte [outLength];
			
			for (i = 0; i < inLength; i++ )
			{
				Input1[i] = Byte.valueOf (s1.substring(i*2,i*2+2), 16);
				Input2[i] = Byte.valueOf (s2.substring(i*2,i*2+2), 16);
			}
		}
		
		public String convert()
		{
			int i = 0;
			StringBuffer s = new StringBuffer(); 
			
			for ( i = 0; i < outLength; i++)
			{
				Output[i] = (byte) ( Input1[i] ^ Input2[i] ); 
			}
			for (byte b:Output)
			{
				s.append(String.format("%x", b));
			}
			return s.toString() ; 
		}
}
