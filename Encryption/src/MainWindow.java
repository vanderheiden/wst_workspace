import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JSeparator;;

public class MainWindow {

	private JFrame frame;
	private JTextField InputField1;
	private JTextField OutputField1;
	private JTextField CheckField1;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_3;
	private JLabel lblNewLabel_4;
	private JLabel lblNewLabel_5;
	private JTextField InputField2a;
	private JTextField InputField2b;
	private JTextField OutputField2;
	private JTextField CheckField2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1024, 1024);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		InputField1 = new JTextField();
		InputField1.setBounds(75, 40, 837, 26);
		frame.getContentPane().add(InputField1);
		InputField1.setColumns(10);
		
		OutputField1 = new JTextField();
		OutputField1.setBounds(75, 80, 501, 26);
		frame.getContentPane().add(OutputField1);
		OutputField1.setColumns(10);

		CheckField1 = new JTextField();
		CheckField1.setBounds(75, 120, 500, 26);
		frame.getContentPane().add(CheckField1);
		CheckField1.setColumns(10);
		
		JLabel lblNewLabel1 = new JLabel("Icon");
		lblNewLabel1.setIcon(new ImageIcon("/Volumes/Data/Users/walter/cdt-master/ws/CryptoSolutions/images/yellow questionmark.png"));
		lblNewLabel1.setBounds(914, 10, 100, 100);
		frame.getContentPane().add(lblNewLabel1);

		JButton btnEncode = new JButton("Encode");
		btnEncode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				hexToBase64 converter1 = new hexToBase64 (InputField1.getText());
				
				OutputField1.setText( converter1.convert());
				
				if ( OutputField1.getText().equals(CheckField1.getText()))
				{
					lblNewLabel1.setIcon(new ImageIcon("/Volumes/Data/Users/walter/cdt-master/ws/CryptoSolutions/images/green tick.png"));					
				}
				else
				{
					lblNewLabel1.setIcon(new ImageIcon("/Volumes/Data/Users/walter/cdt-master/ws/CryptoSolutions/images/red cross.png"));

				}
			}
		});
		btnEncode.setBounds(785, 81, 117, 29);
		frame.getContentPane().add(btnEncode);
		
		lblNewLabel_1 = new JLabel("Crypto Challenge");
		lblNewLabel_1.setFont(new Font("Lucida Grande", Font.PLAIN, 18));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setBounds(163, 10, 512, 26);
		frame.getContentPane().add(lblNewLabel_1);
		
		lblNewLabel_2 = new JLabel("Input");
		lblNewLabel_2.setBounds(6, 45, 61, 16);
		frame.getContentPane().add(lblNewLabel_2);
		
		lblNewLabel_3 = new JLabel("Output");
		lblNewLabel_3.setBounds(6, 85, 61, 16);
		frame.getContentPane().add(lblNewLabel_3);
		
		lblNewLabel_4 = new JLabel("Check");
		lblNewLabel_4.setBounds(6, 125, 61, 16);
		frame.getContentPane().add(lblNewLabel_4);
		
		lblNewLabel_5 = new JLabel("1 - Hex to Base64");
		lblNewLabel_5.setBounds(601, 85, 143, 16);
		frame.getContentPane().add(lblNewLabel_5);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(0, 156, 1024, 12);
		frame.getContentPane().add(separator);
		
		JLabel label = new JLabel("Input");
		label.setBounds(6, 185, 61, 16);
		frame.getContentPane().add(label);
		
		InputField2a = new JTextField();
		InputField2a.setBounds(75, 180, 500, 26);
		frame.getContentPane().add(InputField2a);
		InputField2a.setColumns(10);
		
		JLabel label_1 = new JLabel("Output");
		label_1.setBounds(6, 265, 61, 16);
		frame.getContentPane().add(label_1);
		
		InputField2b = new JTextField();
		InputField2b.setColumns(10);
		InputField2b.setBounds(75, 220, 500, 26);
		frame.getContentPane().add(InputField2b);
		
		OutputField2 = new JTextField();
		OutputField2.setColumns(10);
		OutputField2.setBounds(75, 260, 500, 26);
		frame.getContentPane().add(OutputField2);
		
		JLabel label_2 = new JLabel("Input");
		label_2.setBounds(6, 225, 61, 16);
		frame.getContentPane().add(label_2);
		
		JLabel label_3 = new JLabel("2 - Fixed XOR");
		label_3.setBounds(601, 250, 143, 16);
		frame.getContentPane().add(label_3);

		CheckField2 = new JTextField();
		CheckField2.setColumns(10);
		CheckField2.setBounds(75, 300, 500, 26);
		frame.getContentPane().add(CheckField2);
		
		JLabel label_5 = new JLabel("Check");
		label_5.setBounds(6, 305, 61, 16);
		frame.getContentPane().add(label_5);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(0, 338, 1024, 12);
		frame.getContentPane().add(separator_1);

		JLabel lblNewLabel2 = new JLabel("icon");
		lblNewLabel2.setIcon(new ImageIcon("/Volumes/Data/Users/walter/cdt-master/ws/CryptoSolutions/images/yellow questionmark.png"));
		lblNewLabel2.setBounds(914, 192, 100, 100);
		frame.getContentPane().add(lblNewLabel2);
		
		JButton button = new JButton("Encode");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{	
				fixedXOR converter2 = new fixedXOR(InputField2a.getText(), InputField2b.getText());
				
				OutputField2.setText( converter2.convert());
				
				if ( OutputField2.getText().equals(CheckField2.getText()))
				{
					lblNewLabel2.setIcon(new ImageIcon("/Volumes/Data/Users/walter/cdt-master/ws/CryptoSolutions/images/green tick.png"));					
				}
				else
				{
					lblNewLabel2.setIcon(new ImageIcon("/Volumes/Data/Users/walter/cdt-master/ws/CryptoSolutions/images/red cross.png"));
	
				}
			}
		});
		button.setBounds(785, 245, 117, 29);
		frame.getContentPane().add(button);
		
	}
}
