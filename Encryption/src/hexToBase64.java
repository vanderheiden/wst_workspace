
public class hexToBase64 {
	
	private static char[] base64 = null;
	private byte  Input[];
	private int	 inLength = 0;
	private	int	 outLength = 0;
	public  char Output[];
	
	public hexToBase64(String s)
	{
		int i = 0;
		
		inLength = s.length()/2;
		outLength = ( (inLength * 4 ) +2) / 3;

		Input = new byte [inLength*2];
		for (i = 0; i < inLength; i++ )
		{
			Input[i] = Byte.valueOf (s.substring(i*2,i*2+2), 16);
		}

		// Create the base64 Array (see Wikipedia)
		base64 = new char[64];
		
		i = 0;
		for (char a = 'A'; a <= 'Z'; a++)
		{
			base64[i++] = a;
		}
		for (char a = 'a'; a <= 'z'; a++)
		{
			base64[i++] = a;
		}
		for (char a = '0'; a <= '9'; a++)
		{
			base64[i++] = a;
		}
		base64[i++] = '+';
		base64[i++] = '/';
	}
	
	public String convert()
	{
		int j = 0;
		
		Output = new char[outLength];
		for (int i = 0; i < inLength; i+=3)
		{
			byte	i1 = 0, i2 = 0, i3 = 0;
			byte	o1 = 0, o2 = 0, o3 = 0, o4 = 0;
			
			i1 = Input[i];
			if ( (inLength - i) > 1)
			{
				i2 = Input[i+1];
			}
			if ( (inLength - i) > 2)
			{			
				i3 = Input[i+2];				
			}
			
			o1 = (byte) ((i1 >> 2) & 0x3f);
			o2 = (byte) (((i1 & 0x03) << 4) | ((i2 >> 4) & 0x0f));
			o3 = (byte) (((i2 & 0x0f) << 2) | ((i3 >> 6) & 0x03));
			o4 = (byte) (i3 & 0x3f);
			Output[j++] = base64[o1];
			Output[j++] = base64[o2];
			Output[j++] = base64[o3];
			Output[j++] = base64[o4];
		}
		return  String.valueOf(Output) ;
	}
}
