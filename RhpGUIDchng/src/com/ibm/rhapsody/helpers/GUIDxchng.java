package com.ibm.rhapsody.helpers;

import java.util.Iterator;
import com.telelogic.rhapsody.core.IRPApplication;
import com.telelogic.rhapsody.core.IRPModelElement;
import com.telelogic.rhapsody.core.RPUserPlugin;
import com.telelogic.rhapsody.core.RhapsodyAppServer;

public class GUIDxchng extends RPUserPlugin {
	
	private static IRPApplication	myRhpApp = null;
	
	public static void go (IRPApplication rpyApplication) {
		myRhpApp = rpyApplication;
		myRhpApp.writeToOutputWindow("", "Plugin initialized");				
	}
	
	public static void main(String[] args) {
		IRPApplication localRhpApp = RhapsodyAppServer.getActiveRhapsodyApplication();
		go(localRhpApp);
	}

	@Override
	public void RhpPluginInit(final IRPApplication rpyApplication) {
		// TODO Auto-generated method stub
		IRPApplication localRhpApp;
		
		localRhpApp = rpyApplication;
		go(localRhpApp);
	}

	@Override
	public void RhpPluginInvokeItem() {
		
		String curDesc;
		
		// TODO Auto-generated method stub
		@SuppressWarnings( "unchecked" )
		final Iterator<IRPModelElement> myElements = myRhpApp.activeProject().getNestedElementsRecursive().toList().iterator();

		myRhpApp.writeToOutputWindow("","\n");
		myRhpApp.writeToOutputWindow("","\n");

		while (myElements.hasNext()) {
			final IRPModelElement curElem =  (IRPModelElement) myElements.next();
			 
			myRhpApp.writeToOutputWindow("", curElem.getName());
			myRhpApp.writeToOutputWindow("","\n");
//			myRhpApp.writeToOutputWindow("", curElem.getGUID());
//			myRhpApp.writeToOutputWindow("","\n");
			
			curDesc = curElem.getDescription();
			
			curElem.setGUID("");
//			curElem.setDescription(curDesc);
			
//			myRhpApp.writeToOutputWindow("", curElem.getGUID());
//			myRhpApp.writeToOutputWindow("","\n");
//			myRhpApp.writeToOutputWindow("","\n");
		}
		myRhpApp.writeToOutputWindow("", "GUIDs renewed");
		 
		myRhpApp.activeProject().save(1);		
	}

	@Override
	public void OnMenuItemSelect(String menuItem) {
		// TODO Auto-generated method stub
		RhpPluginInvokeItem();
	}

	public void OnMenuItemSelect() {
		// TODO Auto-generated method stub
		RhpPluginInvokeItem();
	}


	@Override
	public void OnTrigger(String trigger) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean RhpPluginCleanup() {
		// TODO Auto-generated method stub
		// rpyApp.writeToOutputWindow( "Log", "RhpPluginCleanup\n" );

		myRhpApp = null;
		return true;
	}

	@Override
	public void RhpPluginFinalCleanup() {
		// TODO Auto-generated method stub
		
	}

}
 