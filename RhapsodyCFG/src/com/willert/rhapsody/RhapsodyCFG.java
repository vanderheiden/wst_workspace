package com.willert.rhapsody;

import com.telelogic.rhapsody.core.IRPApplication;
import com.telelogic.rhapsody.core.IRPProject;
import com.telelogic.rhapsody.core.RhapsodyAppServer;

public class RhapsodyCFG {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		IRPApplication rpApp;
		IRPProject rpy;
		
		if (( rpApp = RhapsodyAppServer.getActiveRhapsodyApplication() ) != null && 
			( rpy   = rpApp.activeProject()) != null ) {
				rpApp.writeToOutputWindow(null, "Following Model is Loaded: " );
				rpApp.writeToOutputWindow(null, rpy.getName());
				rpApp.writeToOutputWindow(null, "\n" );
		}
		else {
			System.out.println("No Rhapsody Found or");
			System.out.println("No project Found.");
		}
	}

}
